import axios from "axios";


const url = new URL(window.location.href);
const searchParams = url.searchParams
const id=searchParams.get('id');
console.log(id);
//FOR GETTING THE SECOND PAGE FROM BACKEND TO FRONTEND


const title=document.getElementById('title')
const filmthumbnail=document.getElementById('Filmthumbnail')
const aboutthemovie=document.getElementById('Aboutthemovie')
const list=document.getElementById('d')
axios.get('http://localhost:3000/filmdetails/'+id)
.then(function (response) {
    console.log(response)
    title.innerHTML=(response.data.Filmname)
   filmthumbnail.src=response.data.Filmthumbnail
   aboutthemovie.innerHTML=(response.data.Aboutfilm)
  
   list.innerHTML=(response.data.Filmrelease)
  // showFilms(response.data);
 })
 
 .catch(function (error) {
  console.log(error);
 })


 const filmlist=document.getElementById("filmRoot");
 const showFilms= (filmdetails) =>{
   
   for (let i= 0; i<filmdetails.length; i++){
     console.log(filmdetails[i])
 
     const filmDiv = document.createElement("div")
     filmDiv.classList.add('film')
 
     filmDiv.innerHTML = `
     <h2 class=Filmname>${filmdetails[i].Filmname}</h2>
     <img class="Filmthumbnail"src=${filmdetails[i].Filmthumbnail}>
     <button class=Filmrate>${filmdetails[i].Filmrate}</button>
     <h3 class=Filmdimension>${filmdetails[i].Filmdimension}</h3>
     <h3 class=Filmlanguages>${filmdetails[i].Filmlanguages}</h3>
     <h3 class=Filmtime>${filmdetails[i].Filmtime}</h3>
     <h3 class=Filmtype>${filmdetails[i].Filmtype}</h3>
     <h3 class=Filmrelease>${filmdetails[i].Filmrelease}</h3>
     <button class=Filmbookbutton>${filmdetails[i].Filmbookbutton}</button>
     <h3 class=Aboutfilm>${filmdetails[i].Aboutfilm}</h3>
 
    `

    

       filmRoot.append(filmDiv)
 
   }
 }

 //document.getElementById("demo").innerHTML = 
//"The full URL of this page is:<br>" + window.location.href;