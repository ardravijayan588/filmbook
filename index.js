import axios from "axios";

let slideIndex = 0;
showSlides();

function showSlides() {
  let i;
  let slides = document.getElementsByClassName("mySlides");
  let dots = document.getElementsByClassName("dot");
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";  
  }
  slideIndex++;
  if (slideIndex > slides.length) {slideIndex = 1}    
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
  setTimeout(showSlides, 2000); // Change image every 3 seconds
}





//FOR GETTING BACKEND DATA OF MOVIES{POSTERS} TO FRONTEND

axios.get('http://localhost:3000/filmdetails')
  
.then(function (response) {
 showFilms(response.data);

})

.catch(function (error) {
 console.log(error);
})

.finally(function () {

});



//FOR GETTING THE DETAILS OF EVENTS FROM BACKEND TO FRONTEND

axios.get('http://localhost:3000/events')
  
.then(function (response) {
 showEvents(response.data);
})

.catch(function (error) {
 console.log(error);
})

const filmlist=document.getElementById("filmRoot");
 const showFilms= (filmdetails) =>{
   
   for (let i= 0; i<filmdetails.length; i++){
     console.log(filmdetails[i])
 
     const filmDiv = document.createElement("div")
     filmDiv.classList.add('film')
 
     filmDiv.innerHTML = `
     
     <img class="Filmthumbnail"src=${filmdetails[i].Filmthumbnail}>
     <h2 class=Filmname>${filmdetails[i].Filmname}</h2>
     <h3 class=Filmtype>${filmdetails[i].Filmtype}</h3>

`
filmDiv.addEventListener("click",function(){
  const link="bookmovie.html?id="+filmdetails[i]._id
  window.location.href = link
  });
filmRoot.append(filmDiv)
 
   }
  }


  //DISPLAYING THE BACKEND DATA TO FRONTEND(EVENT)
const eventlist=document.getElementById("eventRoot");
const showEvents= (events) =>{
  
  for (let i= 0; i<events.length; i++){
    console.log(events[i])

    const eventDiv = document.createElement("div")
    eventDiv.classList.add('films')

    eventDiv.innerHTML = `

    <img class="Eventthumbnail"src=${events[i].Eventthumbnail}>
    

    
      `
    eventRoot.append(eventDiv)

  }
}




